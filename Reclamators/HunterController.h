// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Hunter.h"
#include "HunterController.generated.h"

USTRUCT()
struct FHunterControllerSettings
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere)
		float CamSpeed;

	FHunterControllerSettings()
		: CamSpeed(3.f)
	{
	}
};

/**
 * 
 */
UCLASS()
class RECLAMATORS_API AHunterController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AHunterController();

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	// End play, used to clear timers
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReasson);

protected:
	virtual void OnPossess(APawn* Pawn) override;

	// How long the sprint button must be held before dodge becomes sprint.
	UPROPERTY(EditAnywhere, Category = "Timings | Sprint")
		float SprintHoldTime;

	UPROPERTY(VisibleAnywhere)
	FString NetModeDebug;

private:

	// Used to call StartSprint when dodge/sprint button is held
	FTimerHandle SprintOverloadTimer;

public:
	// Player-defined settings
	UPROPERTY(EditAnywhere)
	FHunterControllerSettings Settings;

	// Max angle for camera pitch
	UPROPERTY(EditAnywhere)
	float CamPitchMax;
	
	// Min angle for camera pitch
	UPROPERTY(EditAnywhere)
	float CamPitchMin;

	// Pre-casted pointer to AHunter (Replicated)
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	AHunter* Hunter;

	// Input Methods
	// Axes
	void CameraYaw(float AxisValue);
	void CameraPitch(float AxisValue);
	void MoveX(float AxisValue);
	void MoveY(float AxisValue);

	// Actions
	void SprintPressed();
	void SprintReleased();

	void AttackPressed();
	void AttackReleased();

};
