// Fill out your copyright notice in the Description page of Project Settings.


#include "HunterController.h"
#include "Net/UnrealNetwork.h"

AHunterController::AHunterController()

{
	Hunter = nullptr;
	SprintHoldTime = 16.f / 60.f;

	Settings = FHunterControllerSettings();
	CamPitchMin = -85.f;
	CamPitchMax = 45.f;

	switch (GetNetMode())
	{
	case ENetMode::NM_Standalone:
		NetModeDebug = TEXT("Standalone");
		break;

	case ENetMode::NM_DedicatedServer:
		NetModeDebug = TEXT("Dedicated Server");
		break;
	
	case ENetMode::NM_ListenServer:
		NetModeDebug = TEXT("Listen Server");
		break;
	
	case ENetMode::NM_Client:
		NetModeDebug = TEXT("Client");
		break;

	default:
		NetModeDebug = TEXT("ERROR");
		break;
	}
}

void AHunterController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{

}

void AHunterController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AHunterController, Hunter, COND_OwnerOnly);
}

void AHunterController::OnPossess(APawn* Pawn)
{
	Super::OnPossess(Pawn);

	Hunter = Cast<AHunter>(Pawn);
	UE_LOG(LogTemp, Warning, TEXT("Possessed a pawn!"))
	//SetViewTarget(Pawn);
}

void AHunterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("CameraYawAxis", this, &AHunterController::CameraYaw);
	InputComponent->BindAxis("CameraPitchAxis", this, &AHunterController::CameraPitch);
	InputComponent->BindAxis("MoveForwardAxis", this, &AHunterController::MoveX);
	InputComponent->BindAxis("MoveRightAxis", this, &AHunterController::MoveY);

	InputComponent->BindAction("SprintButton", IE_Pressed, this, &AHunterController::SprintPressed);
	InputComponent->BindAction("SprintButton", IE_Released, this, &AHunterController::SprintReleased);
	
	InputComponent->BindAction("AttackShootButton", IE_Pressed, this, &AHunterController::AttackPressed);
	InputComponent->BindAction("AttackShootButton", IE_Released, this, &AHunterController::AttackReleased);
}

void AHunterController::CameraYaw(float AxisValue)
{
	ControlRotation.Yaw += AxisValue * Settings.CamSpeed;

}

void AHunterController::CameraPitch(float AxisValue)
{
	ControlRotation.Pitch = FMath::Clamp(ControlRotation.Pitch + AxisValue * Settings.CamSpeed, CamPitchMin, CamPitchMax);

}

void AHunterController::MoveX(float AxisValue)
{
	if (Hunter)
	{
		GEngine->AddOnScreenDebugMessage(998, 5.f, FColor::Blue, TEXT("MoveX Controlelr"));
		Hunter->MoveStickInput.X = AxisValue;
	}
}

void AHunterController::MoveY(float AxisValue)
{
	if (Hunter)
	{
		Hunter->MoveStickInput.Y = AxisValue;
	}
}


void AHunterController::SprintPressed()
{
	if (Hunter)
	{
		GetWorldTimerManager().SetTimer(SprintOverloadTimer, Hunter, &AHunter::StartSprint, SprintHoldTime, false);
		//Hunter->StartSprint();
	}
}

void AHunterController::SprintReleased()
{
	if (Hunter)
	{
		if (GetWorldTimerManager().IsTimerActive(SprintOverloadTimer))
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Dodge input));
			GetWorldTimerManager().ClearTimer(SprintOverloadTimer);
			Hunter->StartDodge();
		}
		else
		{
			Hunter->StopSprint();
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Stop sprint input"));

		}
	}
}

void AHunterController::AttackPressed()
{
	if (Hunter)
	{

	}
}

void AHunterController::AttackReleased()
{
	if (Hunter)
	{

	}
}