// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "HunterAnimInstance.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class RECLAMATORS_API UHunterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	// bools for anim graph
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	uint32 bIsMoving : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	uint32 bIsJumping : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	uint32 bIsFalling : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	uint32 bIsDodging : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Attack|Weapon")
	uint32 bIsChargingAttack : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Attack|Weapon")
	uint32 bIsAttacking : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Attack|Magick")
	uint32 bIsChargingSpell : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Attack|Magick")
	uint32 bIsCastingSpell : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Items")
	uint32 bIsUsingItem : 1;

	

};
