// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Item.h"
#include "InventoryComponent.generated.h"


// Invetory slots are replicated and contain the class of item, and the quantity held.
USTRUCT()
struct FInventorySlot
{
	GENERATED_BODY()

	// The item held in this slot
	UPROPERTY(EditAnywhere)//, meta = (MustImplement = "Item"))
	TSubclassOf<class UItem> ItemClass;

	// How many of this item are stored
	UPROPERTY(EditAnywhere)
	uint8 Amount;

	FInventorySlot() {}

	FInventorySlot(TSubclassOf<class UItem> InItemClass, uint8 InAmount)
	{
		ItemClass = InItemClass;
		Amount = InAmount;
	}
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RECLAMATORS_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent(const FObjectInitializer& ObjectInitializer);

	// Get replicated propeties
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

protected:
	// Called when the game starts
	//virtual void BeginPlay() override;

private:
	// Item instance pointers
	UPROPERTY()
	TArray<UItem*> ItemInstances; // make this use a cache one day?


	// How many items this invenctory can hold (0 = unlimited)
	UPROPERTY(Replicated, EditDefaultsOnly)
	uint8 MaxSlots;

public:	
	// Called every frame
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// The inventory items
	UPROPERTY(Replicated, VisibleAnywhere)
	TArray<FInventorySlot> ItemSlots;

	// Get the index of an item slot
	UFUNCTION()
	int32 GetIndex(const TSubclassOf<class UItem> ItemClass) const;

	// Check whether this inventory contains an item
	UFUNCTION()
	uint8 GetAmount(const TSubclassOf<class UItem> ItemClass) const;

	// Get an item instance from the inventory
	// @return Pointer to the IItem. nullptr if it is not held
	UFUNCTION()
	UItem* GetItemInstance(const TSubclassOf<class UItem> ItemClass);

	// Add an item to the inventory.
	// @return Whether the item was successfully added
	UFUNCTION()
	bool AddItem(const TSubclassOf<class UItem> ItemClass, const uint8 Amount);

	// Remove an item from the inventory.
	// @return Whether the item was successfully removed
	UFUNCTION()
	bool RemoveItem(const TSubclassOf<class UItem> ItemClass, const uint8 Amount);

	// Remove an item from this inventory, and place it in the supplied inventory.
	// @return Whether the transfer was successful
	UFUNCTION()
	bool TransferItem(const TSubclassOf<class UItem> ItemClass, const uint8 Amount, UInventoryComponent* const OutInventory);
	
};
