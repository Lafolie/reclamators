// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HunterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class RECLAMATORS_API UHunterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
	//=============================================================================
	// Extended UCharacterMovementComponent
	//
	// Replicates the following additional movement modes using FSavedMove_Character::CompressedFlags
	//
	//		FLAG_Custom_0 == Sprint
	//		FLAG_Custom_1 == ExtraMovement (Dodge)
	//		FLAG_Custom_2 == Teleport?
	//		FLAG_Custom_3 == Unused
	//
	//=============================================================================

public:
	UHunterMovementComponent(const FObjectInitializer& ObjectInitializer);
	friend class FSavedMove_Hunter;
	virtual void UpdateFromCompressedFlags(uint8 Flags) override;
	virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;
	virtual void OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity) override;

	//======================================
	// Movement
	//======================================

	// Sprint

	// Base movement multiplier applied to MaxWalkSpeed.
	UPROPERTY(EditAnywhere, Category = "Stats - Movement")
	float BaseMoveSpeedMultiplier;

	// Movement multiplier applied while sprinting.
	UPROPERTY(EditAnywhere, Category = "Stats - Movement")
	float SprintSpeedMultiplier;

	// Acceleration multiplier applied while sprinting.
	UPROPERTY(EditAnywhere, Category = "Stats - Movement")
	float SprintAccelerationMultipler;

	// Enable or disable sprinting
	void SetSprinting(bool bSprinting);

	// If true, try to sprint (or continue sprinting) on next update. If false, try to stop sprinting.
	// Uses compressed flag FLAG_CUSTOM_0
	uint32 bWantsToSprint : 1;

	// Override max speed during sprint
	virtual float GetMaxSpeed() const override;

	// Override max acceleration for sprint
	virtual float GetMaxAcceleration() const override;

	// Launch prediction (dodge, knockback, etc)

	// Strength of force applied when dodging (ground only)
	UPROPERTY(EditAnywhere, Category = "Stats - Movement")
	float DodgeStrength;

	// Send move direction to server (replicated)
	UFUNCTION(Unreliable, Server, WithValidation)
	void ServerSetMoveDirection(const FVector& MoveDir);

	UPROPERTY(VisibleAnywhere)
	FVector ExtraMoveDirection;
	UPROPERTY(VisibleAnywhere)
	uint32 bWantsToDodge : 1;
};



class FSavedMove_Hunter : public FSavedMove_Character
{
public:
	typedef FSavedMove_Character Super;

	// Clears properties in this saved move.
	virtual void Clear() override;

	// Get the compressed flags byte for prediction.
	virtual uint8 GetCompressedFlags() const override;

	// Check whether the move can be combined with NewMove.
	virtual bool CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const override;

	// Set up the move based on the provided character.
	virtual void SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character & ClientData) override;

	// Set up the provided character with the variables stored in the move.
	virtual void PrepMoveFor(class ACharacter* Character) override;

	FVector SavedExtraMoveDirection;

	uint32 bSavedWantsToSprint : 1;
	uint32 bSavedWantsToDodge : 1;
};



class FNetworkPredictionData_Client_Hunter : public FNetworkPredictionData_Client_Character
{
public:
	FNetworkPredictionData_Client_Hunter(const UCharacterMovementComponent& ClientMovement);

	typedef FNetworkPredictionData_Client_Character Super;

	// Allocate a new copy of the Hunter saved move
	virtual FSavedMovePtr AllocateNewMove() override;
};