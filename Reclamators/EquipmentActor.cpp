// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipmentActor.h"
#include "GameFramework/Character.h"

// Sets default values
AEquipmentActor::AEquipmentActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	RootComponent = Mesh;

}

// Called when the game starts or when spawned
void AEquipmentActor::BeginPlay()
{
	Super::BeginPlay();
	
	AActor* Parent = GetParentActor();
	ACharacter* ParentChar = nullptr;
	if (Parent)
	{
		ParentChar = Cast<ACharacter>(Parent);
	}

	if (ParentChar)
	{
		USkeletalMeshComponent* Master = ParentChar->GetMesh();
		if (Master)
		{
			Mesh->SetMasterPoseComponent(Master, false);
		}
	}
}


