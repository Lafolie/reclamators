// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponAttackNotifyState.h"
#include "AnimationDriven.h"
#include "Components/SkeletalMeshComponent.h"

void UWeaponAttackNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	if (!MeshComp)
	{
		return;
	}

	AActor* Owner = MeshComp->GetOwner();
	if (!Owner)
	{
		return;
	}

	IAnimationDriven* AnimDriven = Cast<IAnimationDriven>(Owner);
	if (AnimDriven)
	{
		AnimDriven->ReceiveNotify(EAnimNotify::AN_StartWeapon);
	}
}

void UWeaponAttackNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (!MeshComp)
	{
		return;
	}

	AActor* Owner = MeshComp->GetOwner();
	if (!Owner)
	{
		return;
	}

	IAnimationDriven* AnimDriven = Cast<IAnimationDriven>(Owner);
	if (AnimDriven)
	{
		AnimDriven->ReceiveNotify(EAnimNotify::AN_EndWeapon);
	}
}