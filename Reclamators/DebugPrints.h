// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


/**
 * 
 */
class RECLAMATORS_API DebugPrints
{
public:
	DebugPrints();
	~DebugPrints();

	static void PrintRole(const AActor& Actor);
};
