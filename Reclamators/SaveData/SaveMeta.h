// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "DateTime.h"
#include "SaveMeta.generated.h"

/**
 * 
 */
UCLASS()
class RECLAMATORS_API USaveMeta : public USaveGame
{
	GENERATED_BODY()

public:
	// Which save file this meta relates to
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Meta")
	FString SaveFile;

	// Time this save file was created
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Meta")
	FDateTime SaveTime;

	// Time elasped in game
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Meta")
	FTimespan ElapsedTime;

	// Name of associated character
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Meta")
	FString CharacterName;


};
