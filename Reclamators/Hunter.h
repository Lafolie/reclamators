// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HunterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "InventoryComponent.h"
#include "StatusComponent.h"
#include "Hunter.generated.h"

UCLASS()
class RECLAMATORS_API AHunter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AHunter(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// TEMPORARY

private:


	// Reference to Hunter Movement Component
	UHunterMovementComponent* HunterMove;

public:	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UInventoryComponent* Inventory;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStatusComponent* Status;

	// Sub-actors for character customization

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UChildActorComponent* EquipHead;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UChildActorComponent* EquipChest;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UChildActorComponent* EquipLegs;

	// Skeletal meshes for character customization
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USkeletalMeshComponent* Head;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USkeletalMeshComponent* Hair;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Eyes;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Ears;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mouth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USkeletalMeshComponent* Legs;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Player-controller camera components
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* PlayerCamSpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* PlayerCam;

	// Input control vars
	//FVector2D CamInput;

	UPROPERTY(EditAnywhere)
	FVector MoveStickInput;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UItem> ItemToAdd;

	// Input bindings
	// Axes
	//void CameraYaw(float AxisValue);
	//void CameraPitch(float AxisValue);
	//void MoveX(float AxisValue);
	//void MoveY(float AxisValue);
	// Actions
	void StartSprint();
	void StopSprint();

	void StartDodge();


};
