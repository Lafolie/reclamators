// Fill out your copyright notice in the Description page of Project Settings.


#include "HunterMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"

//======================================
//UHunterMovementComponent
//======================================

UHunterMovementComponent::UHunterMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Constructor
	SprintSpeedMultiplier = 2.f;
	SprintAccelerationMultipler = 2.f;
	bWantsToSprint = false;
	bOrientRotationToMovement = true;
	RotationRate = FRotator(0.f, 720.f, 0.f);
}

void UHunterMovementComponent::OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity)
{
	Super::OnMovementUpdated(DeltaSeconds, OldLocation, OldVelocity);

	if (!CharacterOwner)
	{
		return;
	}

	// Store the movement vec
	if (PawnOwner->IsLocallyControlled())
	{
		ExtraMoveDirection = PawnOwner->GetLastMovementInputVector();
	}

	// Send extra move vec to the server
	if (PawnOwner->Role == ROLE_AutonomousProxy)
	{
		ServerSetMoveDirection(ExtraMoveDirection);
	}

	
	// Perform 'Extra Movement'
	// Only works when grounded for now
	if (bWantsToDodge && IsMovingOnGround())
	{
		ExtraMoveDirection.Normalize();
		FVector DodgeVel = ExtraMoveDirection * DodgeStrength;
		DodgeVel.Z = 0.0f;

		Launch(DodgeVel);
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("DODGE"));

		bWantsToDodge = false;
	}
}

void UHunterMovementComponent::UpdateFromCompressedFlags(uint8 Flags) // CLIENT ONLY
{
	Super::UpdateFromCompressedFlags(Flags);

	bWantsToSprint = (Flags & FSavedMove_Character::FLAG_Custom_0) != 0;
	bWantsToDodge = (Flags & FSavedMove_Character::FLAG_Custom_1) != 0;
}

class FNetworkPredictionData_Client* UHunterMovementComponent::GetPredictionData_Client() const
{
	check(PawnOwner != nullptr);
	//check(PawnOwner->Role < ROLE_Authority);

	if (!ClientPredictionData)
	{
		UHunterMovementComponent* MutableThis = const_cast<UHunterMovementComponent*>(this);

		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_Hunter(*this);
		//MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = NetworkMaxSmoothUpdateDistance;
		//MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = NetworkNoSmoothUpdateDistance;
	}

	return ClientPredictionData;
}

void UHunterMovementComponent::SetSprinting(bool bSprinting)
{
	bWantsToSprint = bSprinting;
}

float UHunterMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	if (bWantsToSprint)
	{
		MaxSpeed *= SprintSpeedMultiplier;
	}

	return MaxSpeed;
}

float UHunterMovementComponent::GetMaxAcceleration() const
{
	float MaxAcceleration = Super::GetMaxAcceleration();

	if (bWantsToSprint)
	{
		MaxAcceleration *= SprintAccelerationMultipler;
	}

	return MaxAcceleration;
}

bool UHunterMovementComponent::ServerSetMoveDirection_Validate(const FVector& MoveDir)
{
	// Boost condition check
	return true;
}

void UHunterMovementComponent::ServerSetMoveDirection_Implementation(const FVector& MoveDir)
{
	ExtraMoveDirection = MoveDir;
}

//======================================
//FSavedMove_Hunter
//======================================

void FSavedMove_Hunter::Clear()
{
	Super::Clear();

	// Clear our custom vars
	bSavedWantsToSprint = false;
	bSavedWantsToDodge = false;
	SavedExtraMoveDirection = FVector::ZeroVector;
}


uint8 FSavedMove_Hunter::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();

	if (bSavedWantsToSprint)
	{
		Result |= FLAG_Custom_0;
	}

	if (bSavedWantsToDodge)
	{
		Result |= FLAG_Custom_1;
	}

	return Result;
}

bool FSavedMove_Hunter::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
{

	// TO-DO:
	// Combine ifs into single compound || condition

	FSavedMove_Hunter* NewMoveHunter = (FSavedMove_Hunter*)& NewMove;
	if (bSavedWantsToSprint != NewMoveHunter->bSavedWantsToSprint)
	{
		return false;
	}

	if (bSavedWantsToDodge != NewMoveHunter->bSavedWantsToDodge)
	{
		return false;
	}

	if (SavedExtraMoveDirection != NewMoveHunter->SavedExtraMoveDirection)
	{
		return false;
	}

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}

// CHAR => SAVEDMOVE
void FSavedMove_Hunter::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character & ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

	UHunterMovementComponent* CharMove = Cast<UHunterMovementComponent>(Character->GetCharacterMovement());
	if (CharMove)
	{
		bSavedWantsToSprint = CharMove->bWantsToSprint;
		bSavedWantsToDodge = CharMove->bWantsToDodge;
		SavedExtraMoveDirection = CharMove->ExtraMoveDirection;
	}
}

// SAVEDMOVE => CHAR
void FSavedMove_Hunter::PrepMoveFor(class ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	UHunterMovementComponent* CharMove = Cast<UHunterMovementComponent>(Character->GetCharacterMovement());
	if (CharMove)
	{
		CharMove->ExtraMoveDirection = SavedExtraMoveDirection;
	}
}

//======================================
//FNetworkPredictionData_Client_Hunter
//======================================

FNetworkPredictionData_Client_Hunter::FNetworkPredictionData_Client_Hunter(const UCharacterMovementComponent& ClientMovement)
	: Super(ClientMovement)
{

}

FSavedMovePtr FNetworkPredictionData_Client_Hunter::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_Hunter());
}