// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "LocalGameInstance.generated.h"

/**
 * Local Game Instance
 */
UCLASS()
class RECLAMATORS_API ULocalGameInstance : public UGameInstance
{
	GENERATED_BODY()
	

	
};
