// Fill out your copyright notice in the Description page of Project Settings.


#include "StatusComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UStatusComponent::UStatusComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	bReplicates = true;
	// ...
}

void UStatusComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UStatusComponent, BaseStats, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UStatusComponent, DerivedStats, COND_OwnerOnly);
	DOREPLIFETIME(UStatusComponent, CurrentStats);
}


// Called when the game starts
void UStatusComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UStatusComponent::DecreaseHP(int32 const Amount)
{
	if (GetOwnerRole() == ENetRole::ROLE_Authority)
	{
		CurrentStats.HP -= (uint32)Amount;
	}
}