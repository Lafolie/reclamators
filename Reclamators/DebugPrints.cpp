// Fill out your copyright notice in the Description page of Project Settings.


#include "DebugPrints.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"

DebugPrints::DebugPrints()
{
}

DebugPrints::~DebugPrints()
{
}

void DebugPrints::PrintRole(const AActor& Actor)
{
	FString StrRole;
	switch (Actor.Role)
	{
	case ENetRole::ROLE_None:
		StrRole = TEXT("None");
		break;

	case ENetRole::ROLE_SimulatedProxy:
		StrRole = TEXT("SimulatedProxy");
		break;

	case ENetRole::ROLE_AutonomousProxy:
		StrRole = TEXT("AutomonousProxy");
		break;

	case ENetRole::ROLE_Authority:
		StrRole = TEXT("Authority");
		break;

	default:
		StrRole = TEXT("ERROR");
		break;
	}
	
	FString Msg = FString::Printf(TEXT("%s local role is %s"), *AActor::GetDebugName(&Actor), *StrRole);
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Emerald, Msg);
}
