// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	MaxSlots = 20;
}


// Replication
void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UInventoryComponent, ItemSlots, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UInventoryComponent, MaxSlots, COND_OwnerOnly);
}

// Called when the game starts`
//void UInventoryComponent::BeginPlay()
//{
//	//Super::BeginPlay();
//
//	// ...
//	
//}

int32 UInventoryComponent::GetIndex(const TSubclassOf<class UItem> ItemClass) const
{
	int32 Index = ItemSlots.IndexOfByPredicate([&](const FInventorySlot& Slot)
	{
		return Slot.ItemClass == ItemClass;
	});

	return Index;
}

uint8 UInventoryComponent::GetAmount(const TSubclassOf<class UItem> ItemClass) const
{
	const FInventorySlot* Slot = ItemSlots.FindByPredicate([&](const FInventorySlot& Slot)
	{
		return Slot.ItemClass == ItemClass;
	});

	return (Slot) ? Slot->Amount : 0;

}

bool UInventoryComponent::AddItem(const TSubclassOf<class UItem> ItemClass, const uint8 Amount)
{

	if (GetOwnerRole() < ROLE_Authority)
	{
		return false;
	}

	if (ItemSlots.Num() < MaxSlots)
	{
		ItemSlots.Add(FInventorySlot(ItemClass, Amount));
		
		return true;
	}
	else
	{
		return false;
	}
}

bool UInventoryComponent::RemoveItem(const TSubclassOf<class UItem> ItemClass, const uint8 Amount)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		return false;
	}

	int32 Index = GetIndex(ItemClass);
	if (Index == INDEX_NONE)
	{
		return false;
	}

	FInventorySlot* Slot = &ItemSlots[Index];
	uint8 Amt = Slot->Amount;
	if (Amt< Amount)
	{
		return false;
	}
	else if (Amt == Amount)
	{
		ItemSlots.RemoveAt(Index);

		return true;
	}
	else
	{
		Slot->Amount -= Amount;

		return true;
	}
}

bool UInventoryComponent::TransferItem(const TSubclassOf<class UItem> ItemClass, const uint8 Amount, UInventoryComponent* const OutInventory)
{
		if (GetOwnerRole() < ROLE_Authority)
	{
		return false;
	}
	// Add the items first
	if (OutInventory->AddItem(ItemClass, Amount))
	{
		if (RemoveItem(ItemClass, Amount))
		{
			// Items transferred
			return true;
		}
		else
		{
			// Remove the newly added items
			OutInventory->RemoveItem(ItemClass, Amount);
			return false;
		}
	}

	return false;
}

UItem* UInventoryComponent::GetItemInstance(const TSubclassOf<class UItem> ItemClass)
{
	return NewObject<UItem>();
}