// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Item.generated.h"

/**
 * Base item class
 */
UCLASS(Abstract, Blueprintable)
class RECLAMATORS_API UItem : public UObject
{
	GENERATED_BODY()
public:
	UItem();

public:
	// In-game name
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UI | Text")
	FText Name;

	// In-game description
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI | Text")
	FText Description;

	// Base monetary value
	UPROPERTY(EditDefaultsOnly, Category = "Item Stats")
	uint32 BaseValue;

	UFUNCTION()
	virtual uint32 GetValue() const;

	// Material for use in menus
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI | Icon")
	UMaterialInstance* IconMaterial;


};
