// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EquipmentActor.h"
#include "EquipmentHead.generated.h"

/**
 * 
 */
UCLASS(Abstract, Blueprintable)
class RECLAMATORS_API AEquipmentHead : public AEquipmentActor
{
	GENERATED_BODY()
	
public:
	AEquipmentHead();

	UPROPERTY(EditDefaultsOnly)
	uint8 bHidesHair : 1;

	UPROPERTY(EditDefaultsOnly)
	uint8 bHidesEyes : 1;

	UPROPERTY(EditDefaultsOnly)
	uint8 bHidesEars : 1;

	UPROPERTY(EditDefaultsOnly)
	uint8 bHidesMouth : 1;
};
