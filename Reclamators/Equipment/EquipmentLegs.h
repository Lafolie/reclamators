// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EquipmentActor.h"
#include "EquipmentLegs.generated.h"

/**
 * 
 */
UCLASS(Abstract, Blueprintable)
class RECLAMATORS_API AEquipmentLegs : public AEquipmentActor
{
	GENERATED_BODY()
	
};
