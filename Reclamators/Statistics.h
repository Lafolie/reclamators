// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Statistics.generated.h"


UENUM(BlueprintType)
enum class EAspectElement : uint8
{
	AE_Earth	UMETA(DisplayName = "Earth"),
	AE_Wind		UMETA(DisplayName = "Wind"),
	AE_Fire		UMETA(DisplayName = "Fire"),
	AE_Water	UMETA(DisplayName = "Water"),
	AE_Ice		UMETA(DisplayName = "Ice"),
	AE_Electric UMETA(DisplayName = "Electric"),
	AE_Light	UMETA(DisplayName = "Light"),
	AE_Dark		UMETA(DisplayName = "Dark"),
	AE_Force	UMETA(DisplayName = "Force"),
	AE_Slash	UMETA(DisplayName = "Slash"),
	AE_Piercing UMETA(DisplayName = "Piercing"),
	AE_Blunt	UMETA(DisplayName = "Blunt"),
	AE_Explosive UMETA(DisplayName = "Explosive"),
	AE_Breath	UMETA(DisplayName = "Breath"),
	AE_Balance	UMETA(DisplayName = "Balance"),
	AE_Chaos	UMETA(DisplayName = "Chaos"),
	AE_COUNT	UMETA(DisplayName = "Count", Hidden)
};

USTRUCT()
struct FAspectTable
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float Earth;

	UPROPERTY(EditAnywhere)
	float Wind;

	UPROPERTY(EditAnywhere)
	float Fire;

	UPROPERTY(EditAnywhere)
	float Water;

	UPROPERTY(EditAnywhere)
	float Ice;

	UPROPERTY(EditAnywhere)
	float Electric;

	UPROPERTY(EditAnywhere)
	float Light;

	UPROPERTY(EditAnywhere)
	float Dark;

	UPROPERTY(EditAnywhere)
	float Force;

	UPROPERTY(EditAnywhere)
	float Slash;

	UPROPERTY(EditAnywhere)
	float Piercing;

	UPROPERTY(EditAnywhere)
	float Blunt;

	UPROPERTY(EditAnywhere)
	float Explosive;

	UPROPERTY(EditAnywhere)
	float Breath;

	UPROPERTY(EditAnywhere)
	float Balance;

	UPROPERTY(EditAnywhere)
	float Chaos;

};

USTRUCT()
struct FCharacterStats
{
	GENERATED_BODY()

	// Hit Points
	UPROPERTY(EditAnywhere, Category = "Resource Points")
	uint32 HP;

	UPROPERTY(EditAnywhere, Category = "Resource Points")
	uint32 MaxHP;

	// Magick Points
	UPROPERTY(EditAnywhere, Category = "Resource Points")
	uint32 MP;

	UPROPERTY(EditAnywhere, Category = "Resource Points")
	uint32 MaxMP;

	// Stamina Points
	UPROPERTY(EditAnywhere, Category = "Resource Points")
	uint32 SP;

	UPROPERTY(EditAnywhere, Category = "Resource Points")
	uint32 MaxSP;

	// Attack strength
	UPROPERTY(EditAnywhere, Category = "Stats")
	uint32 Attack;

	// Defense strength
	UPROPERTY(EditAnywhere, Category = "Stats")
	uint32 Defense;

	// Damage aspect coefs
	UPROPERTY(EditAnywhere, Category = "Aspects")
	FAspectTable WeakResist;


	FCharacterStats()
		: HP(100),
		MaxHP(100),
		MP(100),
		MaxMP(100),
		SP(100),
		MaxSP(100),
		Attack(25),
		Defense(25)
	{

		//for (uint8 i = (uint8)EAspectElement::AE_Earth; i != (uint8)EAspectElement::AE_COUNT; i++)
		//{
		//	WeakResist.Emplace(1.f);
		//	WeakResistEditor.Emplace((EAspectElement)i, 1.f);
		//}
	}

};

/**
 * RPG Character Statistics
 */
UCLASS()
class RECLAMATORS_API UStatistics : public UObject
{
	GENERATED_BODY()

public:
	static void Dummy() {};
	
};
