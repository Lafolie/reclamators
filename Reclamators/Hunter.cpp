// Fill out your copyright notice in the Description page of Project Settings.


#include "Hunter.h"
#include "DebugPrints.h"
//#include "Components/SkeletalMeshComponent.h"

// Sets default values
AHunter::AHunter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UHunterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationYaw = false;

	// Setup ccomponents
	// Camera
	PlayerCamSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("PlayerCamSpringArm"));
	PlayerCamSpringArm->SetupAttachment(RootComponent);
	PlayerCamSpringArm->SetRelativeLocationAndRotation(FVector(0.f, 0.f, 0.f), FRotator(-10.f, 0.f, 0.f));
	PlayerCamSpringArm->TargetArmLength = 500.f;
	PlayerCamSpringArm->bEnableCameraLag = true;
	PlayerCamSpringArm->CameraLagSpeed = 3.f;
	PlayerCamSpringArm->SetIsReplicated(false);
	PlayerCamSpringArm->bUsePawnControlRotation = true;
	//PlayerCamSpringArm->SetAbsolute(false, true, false);

	PlayerCam = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCam"));
	PlayerCam->SetupAttachment(PlayerCamSpringArm, USpringArmComponent::SocketName);
	PlayerCam->SetIsReplicated(false);

	// Inventory
	Inventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("Inventory"));
	Inventory->SetIsReplicated(true);

	// Status
	Status = CreateDefaultSubobject<UStatusComponent>(TEXT("Status"));
	Status->SetIsReplicated(true);

	USceneComponent* Mesh = Cast<USceneComponent>(GetMesh());
	Mesh->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -90.f), FRotator(0.f, -90.f, 0.f));

	// Equip actors
	EquipHead = CreateDefaultSubobject<UChildActorComponent>(TEXT("EquipHead"));
	EquipChest = CreateDefaultSubobject<UChildActorComponent>(TEXT("EquipChest"));
	EquipLegs = CreateDefaultSubobject<UChildActorComponent>(TEXT("EquipLegs"));

	EquipHead->SetupAttachment(Mesh);
	EquipChest->SetupAttachment(Mesh);
	EquipLegs->SetupAttachment(Mesh);

	// Meshes
	USkeletalMeshComponent* Chest = GetMesh();
	
	Head = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HeadMesh"));
	Head->SetupAttachment(Chest);
	Head->SetMasterPoseComponent(Chest, false);
	Legs = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("LegsMesh"));
	Legs->SetupAttachment(Chest);
	Legs->SetMasterPoseComponent(Chest, false);

	Hair = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HairMesh"));
	Hair->SetupAttachment(Head, TEXT("Hair"));
	Eyes = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EyesMesh"));
	Eyes->SetupAttachment(Head, TEXT("Eyes"));
	Ears = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EarsMesh"));
	Ears->SetupAttachment(Head, TEXT("Ears"));
	Mouth = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MouthMesh"));
	Mouth->SetupAttachment(Head, TEXT("Mouth"));

	HunterMove = Cast<UHunterMovementComponent>(GetCharacterMovement());
}

// Called when the game starts or when spawned
void AHunter::BeginPlay()
{
	Super::BeginPlay();

	if (IsLocallyControlled()) //&& Cast<APlayerController>(Ctrl) == GEngine->GetFirstLocalPlayerController(GetWorld()))
	{
		DebugPrints::PrintRole(*this);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(995, 5.f, FColor::Purple, "Not locally controlled!");
	}

	if (HasAuthority() && ItemToAdd)
	{
		Inventory->AddItem(ItemToAdd, 6);
		Inventory->RemoveItem(ItemToAdd, 6);
	}

}

// Called every frame
void AHunter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Movement Update
	if (Role > ROLE_SimulatedProxy)
	{
		FString Msg = MoveStickInput.ToString();
		GEngine->AddOnScreenDebugMessage(997, 5.f, FColor::Yellow, Msg);

		FRotator Rot = FRotator(0.f, GetControlRotation().Yaw, 0.f);
		FVector Forward = Rot.RotateVector(MoveStickInput);
		AddMovementInput(Forward, MoveStickInput.Size());
		Msg = Forward.ToString();
		GEngine->AddOnScreenDebugMessage(996, 5.f, FColor::Orange, Msg);
	}

	FString Msg = IsLocallyControlled() ? TEXT("Local controlled") : TEXT("Not local controlled");
	GEngine->AddOnScreenDebugMessage(994, 5.f, FColor::Orange, Msg);

	FString CtrlName = GetDebugName((AActor*)Controller);
	GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Orange, CtrlName);
}

// Called to bind functionality to input
//void AHunter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
//{
//	//Super::SetupPlayerInputComponent(PlayerInputComponent);
//	//PlayerInputComponent->BindAxis("CameraYawAxis", this, &AHunter::CameraYaw);
//	//PlayerInputComponent->BindAxis("CameraPitchAxis", this, &AHunter::CameraPitch);
//	//PlayerInputComponent->BindAxis("MoveForwardAxis", this, &AHunter::MoveX);
//	//PlayerInputComponent->BindAxis("MoveRightAxis", this, &AHunter::MoveY);
//
//	//PlayerInputComponent->BindAction("SprintButton", IE_Pressed, this, &AHunter::SprintPressed);
//	//PlayerInputComponent->BindAction("SprintButton", IE_Released, this, &AHunter::SprintReleased);
//}

//void AHunter::CameraYaw(float AxisValue)
//{
//	CamInput.X = AxisValue;
//}
//
//void AHunter::CameraPitch(float AxisValue)
//{
//	CamInput.Y = AxisValue;
//}
//
//void AHunter::MoveX(float AxisValue)
//{
//	GEngine->AddOnScreenDebugMessage(998, 5.f, FColor::Blue, TEXT("MoveX"));
//	MoveStickInput.X = AxisValue;
//}
//
//void AHunter::MoveY(float AxisValue)
//{
//	MoveStickInput.Y = AxisValue;
//}

void AHunter::StartSprint()
{
	GEngine->AddOnScreenDebugMessage(999, 5.f, FColor::Emerald, TEXT("Sprint down"));
	if (HunterMove)
	{
		HunterMove->SetSprinting(true);
	}
}

void AHunter::StopSprint()
{
	GEngine->AddOnScreenDebugMessage(999, 5.f, FColor::Emerald, TEXT("Sprint up"));

	if (HunterMove)
	{
		HunterMove->SetSprinting(false);
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Sprint input"));
	}
}

void AHunter::StartDodge()
{
	if (HunterMove)
	{
		HunterMove->bWantsToDodge = true;
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Dodge input"));
	}
}