// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Ability.generated.h"

USTRUCT()
struct FAbilityInvoker
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float Delay;
};

/**
 * 
 */
UCLASS(Abstract, Blueprintable)
class RECLAMATORS_API UAbility : public UObject
{
	GENERATED_BODY()

public:
	UAbility();

	// In-game name
	UPROPERTY(EditDefaultsOnly, Category = "UI|Text")
	FText Name;
	
	// In-game description
	UPROPERTY(EditDefaultsOnly, Category = "UI|Text")
	FText Description;

	// Material for use in menus
	UPROPERTY(EditDefaultsOnly, Category = "UI|Icon")
	UMaterialInstance* IconMaterial;

	// Cooldown time
	UPROPERTY(EditAnywhere, Category = "Stats")
	float Cooldown;

	// Time between starting and firing ability function
	UPROPERTY(EditAnywhere, Category = "Stats")
	float CastTime;

	// Ability cancelling

	// Abilities that this ability can be cancelled into.
	// If @bInvereCancelLinkers is enables, an empty list here means the ability can cancel into anything.
	UPROPERTY(EditAnywhere, Category = "Cancelling")
	TArray<UAbility*> CancelWhitelist;

	UPROPERTY(EditAnywhere, Category = "Cancelling|Flags")
	uint8 bCanBeCancelled : 1;

	// Makes CancelWhitelist into a blacklist
	UPROPERTY(EditAnywhere, Category = "Cancelling|Flags")
	uint8 bInverseCancelWhitelist : 1;
};
