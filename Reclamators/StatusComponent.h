// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Statistics.h"
#include "StatusComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RECLAMATORS_API UStatusComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStatusComponent();

	// Base intrinsic statistics
	UPROPERTY(EditAnywhere, Replicated)
	FCharacterStats BaseStats;

	// Base stats after loadout changes
	UPROPERTY(EditAnywhere, Replicated)
	FCharacterStats DerivedStats;

	// Current stats used in damage/heal routines
	UPROPERTY(EditAnywhere, Replicated)
	FCharacterStats CurrentStats;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	UFUNCTION(BlueprintCallable)
	void DecreaseHP(int32 const Amount);
		
};
