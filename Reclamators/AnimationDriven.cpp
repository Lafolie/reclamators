// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimationDriven.h"

// Add default functionality here for any IAnimationDriven functions that are not pure virtual.

float IAnimationDriven::GetIsMoving()
{
	return 0.f;
}

EJumpFall IAnimationDriven::GetIsJumpingOrFalling()
{
	return EJumpFall::JF_None;
}

bool IAnimationDriven::GetIsDodging()
{
	return false;
}
